GPU=$1
python3 draw_gradcam.py --gpu $GPU \
--model_dir /data/dongchengbo/VisualSearch/muti_dataset/dfdc_dfv2_ff++_timit_bilibili_celeb5_opensoftware_facehq_train/models/dfdc_dfv2_ff++_timit_bilibili_celeb5_opensoftware_facehq_val/addhq/efficientnet-b7-ns_lr0.005decay0.8_multidata.json/run_0/model_2.pth.tar \
--save_dir /data/dongchengbo/VisualSearch/Faces-HQ_retina_face/ImageData \
--draw_list /data/dongchengbo/VisualSearch/Faces-HQ_test.txt