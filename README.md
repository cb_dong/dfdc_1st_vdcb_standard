# A 3rd-party Implemented DFDC-1st for Deepfake Detection

## Environment

+ Ubuntu 16.04.6 LTS
+ Python 3.5.2

## Requirement
+ Install nvidia-apex follow https://github.com/NVIDIA/apex, move it here.
+ pip install requirements.txt
+ bash download_weights.sh

## Usage


### Training
Change following in run_train.sh
```
data_path, run_id, config_name, train_collection, val_collection, test_collection
```
Then run 
```bash 
bash run_train.sh gpu_id additional_description
e.g. bash run_train.sh 0 Vfinetune
```
### Prediction
for img: 

```bash
bash run_pred_img.sh gpu_id if_submision
e.g. bash run_pred_img.sh 0 1 
```
for video:
```bash
bash run_pred_video.sh 0
```
To use pretrained models, get them from 

链接：https://pan.baidu.com/s/1fepyjxcCn6zyorAlQyvTQg 提取码：i1v9

and change model path in run_pred_xx.sh
### Visualization (via gradcam)  
```bash
bash run_gradcam.sh gpu_id
```