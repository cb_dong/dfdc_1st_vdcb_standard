import cv2
from transforms import direct_val
from models.classifier import DeepFakeClassifier
import torch
import re
import numpy as np
import os
os.environ['CUDA_VISIBLE_DEVICES']='3'


class GAN_opsoft_classfify(object):
    def __init__(self,model_path,device,half):
        self.model = DeepFakeClassifier(encoder="tf_efficientnet_b7_ns",num_classes=5)
        self.transforms = direct_val
        self.model_path = model_path
        self.device = device
        self.half = half


    def predict(self,img_path):
        img_raw=cv2.imread(img_path)
        inputs = self.transforms([img_raw],380)
        inputs = inputs.to(self.device)
        with torch.no_grad():
            if self.half:
                output = self.model(inputs.half())
            else:
                output = self.model(inputs)
            probs = torch.softmax(output, dim=1).cpu().numpy()
        return probs

    def init_model(self):
        print("loading state dict %s"%self.model_path)
        try:
            checkpoint = torch.load(self.model_path, map_location="cpu")
            state_dict = checkpoint.get("state_dict", checkpoint)
            self.model.load_state_dict({re.sub("^module.", "", k): v for k, v in state_dict.items()}, strict=False)
        except:
            print("error loading %s"%self.model_path)
        self.model = self.model.to(self.device)
        self.model.eval()
        if self.half:
            self.model = self.model.half()
        del checkpoint

if __name__ == '__main__':


    gan_opsoft_classfify = GAN_opsoft_classfify(
        model_path='/data/yangtianyun/dataset/GANFingerprints/gan_opsf_yty_train/models/gan_opsf_yty_val/efficientnet-b7-ns_lr0.01decay0.8_opsoft.json/run_0/model_best_dice.pth.tar',
        device=torch.device('cuda'),
        half=True
    )
    gan_opsoft_classfify.init_model()

    img_path='/data/yangtianyun/dataset/notdetected/image_face/img/IMG_6134.png'

    prob = gan_opsoft_classfify.predict(img_path)[0]
    pred_label = np.argmax(prob, axis=0)
    labels=['real','融合','贴图','变老/变年轻','gan']
    result = labels[pred_label]
    print(img_path)
    print('检测结果为:', result)
    print('检测为%s的概率为: %.4f' % (result,prob[pred_label]))





