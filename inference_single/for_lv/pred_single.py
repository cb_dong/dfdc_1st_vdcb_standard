import cv2
from transforms import direct_val
from models.classifier import DeepFakeClassifier
import torch
import re
import numpy as np
from PIL import Image
import torchvision.transforms as transforms
import os
from tqdm import tqdm
from sklearn.metrics import confusion_matrix
os.environ['CUDA_VISIBLE_DEVICES']='3'


class GAN_classify():
    def __init__(self,model_path,device):
        self.model_path = model_path
        self.device = device

    def init_model(self):
        self.net = torch.load(self.model_path, map_location='cpu')
        self.net = self.net.to(self.device)
        self.net.eval()

    def predict(self,img_path):
        img = Image.open(img_path)
        norm_transform = transforms.Compose([
            transforms.ToTensor(),
            transforms.Normalize([0.5, 0.5, 0.5], [0.5, 0.5, 0.5]),
        ])

        try:
            RandomCrop = transforms.RandomCrop(size=(224, 224))
            img = RandomCrop(img)
            img = norm_transform(img)
        except:
            img = norm_transform(img)
            #print("xxxxxx")
        img = torch.unsqueeze(img, 0)
        img = img.to(self.device)
        output = self.net(img)
        probs = output.cpu().detach().numpy()
        return probs


class GAN_opsoft_classfify(object):
    def __init__(self,model_path,device,half):
        self.model = DeepFakeClassifier(encoder="tf_efficientnet_b7_ns",num_classes=5)
        self.transforms = direct_val
        self.model_path = model_path
        self.device = device
        self.half = half


    def predict(self,img_path):
        img_raw=cv2.imread(img_path)
        inputs = self.transforms([img_raw],380)
        inputs = inputs.to(self.device)
        with torch.no_grad():
            if self.half:
                output = self.model(inputs.half())
            else:
                output = self.model(inputs)
            probs = torch.softmax(output, dim=1).cpu().numpy()
        return probs

    def init_model(self):
        print("loading state dict %s"%self.model_path)
        try:
            checkpoint = torch.load(self.model_path, map_location="cpu")
            state_dict = checkpoint.get("state_dict", checkpoint)
            self.model.load_state_dict({re.sub("^module.", "", k): v for k, v in state_dict.items()}, strict=False)
        except:
            print("error loading %s"%self.model_path)
        self.model = self.model.to(self.device)
        self.model.eval()
        if self.half:
            self.model = self.model.half()
        del checkpoint

if __name__ == '__main__':



    gan_opsoft_classfify = GAN_opsoft_classfify(
        model_path='/data/yangtianyun/dataset/GANFingerprints/gan_opsf_yty_train/models/gan_opsf_yty_val/efficientnet-b7-ns_lr0.01decay0.8_opsoft.json/run_0/model_best_dice.pth.tar',
        device=torch.device('cuda'),
        half=True
    )
    gan_opsoft_classfify.init_model()

    gan_classify = GAN_classify(
        model_path='/data/jijiaqi/datasets/GANFingerprints/ProStyleStyle2_yty_train/models/ProStyleStyle2_yty_val/resnet50_balance_random_crop_224_f1/run_0/model.pth.tar',
        device=torch.device('cuda')
    )
    gan_classify.init_model()


    with open('/data/dongchengbo/VisualSearch/GAB_test/boss_img.txt','r') as f:
        imgs = f.readlines()
    imgs = [each.strip() for each in imgs]
    gts = [int(each.split()[-1]) for each in imgs]
    imgs = [each.split()[0] for each in imgs]
    pred = []
    print(len(imgs))
    for img_path in tqdm(imgs):

        #print(img_path)
        prob = gan_opsoft_classfify.predict(img_path)[0]
        pred_label = np.argmax(prob, axis=0)
        labels=['real','融合','贴图','变老/变年轻','gan']
        result = labels[pred_label]
        score = prob[pred_label]
        # print('pred prob:', prob)
        # print('pred type:',labels[pred_label])

        if pred_label==4:
            prob_gan=prob[4]
            prob=gan_classify.predict(img_path)[0]*prob_gan
            labels=['real','stylegan','stylegan2','progan']
            pred_label = np.argmax(prob, axis=0)
            result=labels[pred_label]
            score = prob[pred_label]
            # print('pred probs:', prob)
            # print('pred type:',labels[pred_label])
        if result == 'real':
            pred.append(1-score)
        else:
            pred.append(score)
    import pdb
    pdb.set_trace()
    gts = np.array(gts)
    pred = np.array(pred)
    pred_label = 1.0 * (pred > 0.5)
    acc = 1.0 * np.sum(pred_label == gts) / gts.shape[0]
    tn, fp, fn, tp = confusion_matrix(gts, pred_label).reshape(-1)
    sen = float(tp) / (tp + fn + 1e-8)
    spe = float(tn) / (tn + fp + 1e-8)
    print(acc, sen, spe)
        #
            # print('检测结果为:', result)
            # print('检测为%s的概率为: %.4f' % (result,prob[pred_label]))




