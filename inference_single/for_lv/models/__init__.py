# -*- coding: utf-8 -*-
"""
Created on 2019/8/4 上午9:37

@author: mick.yi

"""

import torch.nn as nn
import torch.nn.functional as F
import torchvision
import torch

class ResNet(nn.Module):
    def __init__(self, name, class_num=4):
        super(ResNet, self).__init__()
        if '50' in name:
            res = torchvision.models.resnet50(pretrained=True)
        elif '101' in name:
            res = torchvision.models.resnet101(pretrained=True)
        else:
            raise Exception('invalid ResNet model {}'.format(name))

        self.fconv = nn.Sequential(*list(res.children())[:4])
        self.layer1 = res.layer1
        self.layer2 = res.layer2
        self.layer3 = res.layer3
        self.layer4 = res.layer4
        self.avgpool = res.avgpool
        self.fc = nn.Sequential(
            nn.Linear(2048, class_num, bias=True),
            nn.Dropout(0.5),
        )

    def forward(self, x):
        # print(x.shape)
        # ([48, 3, 224, 224]))
        x = self.fconv(x)
        x = self.layer1(x)
        x = self.layer2(x)
        x = self.layer3(x)
        x = self.layer4(x)
        x = self.avgpool(x)
        x = x.view(x.size(0), -1)
        out = self.fc(x)
        out = torch.softmax(out,dim=1)
        return out


class VGGNet(nn.Module):
    def __init__(self, vgg):
        super(VGGNet, self).__init__()
        self.vgg = vgg
        self.features = vgg.features
        self.avgpool = vgg.avgpool
        self.fc = nn.Sequential(
            nn.Linear(25088, 4096, True),
            nn.ReLU(inplace=True),
            nn.Dropout(0.5),
            nn.Linear(4096, 512, True),
            nn.ReLU(inplace=True),
            nn.Dropout(0.5),
            nn.Linear(512, 1, True),
        )

    def forward(self, x):
        x = self.features(x)
        x = self.avgpool(x)
        x = x.view(x.size(0), -1)
        x = self.fc(x)
        return x

    def load_pretrained(self):
        pretrained_dict = self.vgg.state_dict()
        model_dict = self.features.state_dict()
        pretrained_dict = {k: v for k, v in pretrained_dict.items() if k in model_dict}
        model_dict.update(pretrained_dict)
        self.features.load_state_dict(model_dict)



