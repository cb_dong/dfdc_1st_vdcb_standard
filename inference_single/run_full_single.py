import cv2
import re
from functools import partial
from timm.models.efficientnet import tf_efficientnet_b7_ns
import torch
from torch import nn
from torch.nn.modules.dropout import Dropout
from torch.nn.modules.linear import Linear
from torch.nn.modules.pooling import AdaptiveAvgPool2d
from albumentations import DualTransform, Compose,  PadIfNeeded
from albumentations.pytorch.functional import img_to_tensor
import numpy as np
from retina_face_extractor import RetinaFaceExtractorVideo
import pdb
"""
模型定义
"""
encoder_params = {
    "tf_efficientnet_b7_ns": {
        "features": 2560,
        "init_op": partial(tf_efficientnet_b7_ns, pretrained=True, drop_path_rate=0.2)
    },
}

class DeepFakeClassifier(nn.Module):
    def __init__(self, encoder, dropout_rate=0.0) -> None:
        super().__init__()
        self.encoder = encoder_params[encoder]["init_op"]()
        self.avg_pool = AdaptiveAvgPool2d((1, 1))
        self.dropout = Dropout(dropout_rate)
        self.fc = Linear(encoder_params[encoder]["features"], 1)

    def forward(self, x):
        x = self.encoder.forward_features(x)
        x = self.avg_pool(x).flatten(1)
        x = self.dropout(x)
        x = self.fc(x)
        return x


"""
transform
"""
def isotropically_resize_image(img, size, interpolation_down=cv2.INTER_AREA, interpolation_up=cv2.INTER_CUBIC):
    h, w = img.shape[:2]
    if max(w, h) == size:
        return img
    if w > h:
        scale = size / w
        h = h * scale
        w = size
    else:
        scale = size / h
        w = w * scale
        h = size
    interpolation = interpolation_up if scale > 1 else interpolation_down
    resized = cv2.resize(img, (int(w), int(h)), interpolation=interpolation)
    return resized

class IsotropicResize(DualTransform):
    def __init__(self, max_side, interpolation_down=cv2.INTER_AREA, interpolation_up=cv2.INTER_CUBIC,
                 always_apply=False, p=1):
        super(IsotropicResize, self).__init__(always_apply, p)
        self.max_side = max_side
        self.interpolation_down = interpolation_down
        self.interpolation_up = interpolation_up

    def apply(self, img, interpolation_down=cv2.INTER_AREA, interpolation_up=cv2.INTER_CUBIC, **params):
        return isotropically_resize_image(img, size=self.max_side, interpolation_down=interpolation_down,
                                          interpolation_up=interpolation_up)

    def apply_to_mask(self, img, **params):
        return self.apply(img, interpolation_down=cv2.INTER_NEAREST, interpolation_up=cv2.INTER_NEAREST, **params)

    def get_transform_init_args_names(self):
        return ("max_side", "interpolation_down", "interpolation_up")

def create_val_transforms(size=300):
    return Compose([
        IsotropicResize(max_side=size, interpolation_down=cv2.INTER_AREA, interpolation_up=cv2.INTER_CUBIC),
        PadIfNeeded(min_height=size, min_width=size, border_mode=cv2.BORDER_CONSTANT),
    ])

def direct_val(imgs,size):
    #img 输入为RGB顺序
    imgs = [cv2.cvtColor(img, cv2.COLOR_BGR2RGB) for img in imgs]
    transforms = create_val_transforms(size)
    normalize = {"mean": [0.485, 0.456, 0.406],
                 "std": [0.229, 0.224, 0.225]}
    imgs = [img_to_tensor(transforms(image=each)['image'],normalize).unsqueeze(0) for each in imgs]
    imgs = torch.cat(imgs)

    return imgs


def batch_sort(faces, scores_all, ori_coordinates, coordinates, landmarks):
    #按照面积划分
    sort_base = [(face_coord[3] - face_coord[1]) * (face_coord[2] - face_coord[0]) for face_coord in ori_coordinates]
    sort_index = np.argsort(sort_base)
    sort_index = sort_index[::-1]

    faces_new = []
    scores_all_new = []
    ori_coordinates_new = []
    coordinates_new = []
    landmarks_new = []
    for index in sort_index:
        faces_new.append(faces[index])
        scores_all_new.append(scores_all[index])
        ori_coordinates_new.append(ori_coordinates[index])
        coordinates_new.append(coordinates[index])
        landmarks_new.append(landmarks[index])
    return faces_new, scores_all_new, ori_coordinates_new, coordinates_new, landmarks_new

def check_blur(image):
    image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    blur_map = cv2.Laplacian(image, cv2.CV_64F)
    score = np.var(blur_map)
    return score < 800


"""
inference
"""
class Inference_Single(object):
    def __init__(self,model_path,device,half):
        self.model = DeepFakeClassifier(encoder="tf_efficientnet_b7_ns")
        self.transforms = direct_val
        self.model_path = model_path
        self.device = device
        self.half = half
        self.face_extractor = RetinaFaceExtractorVideo(self.device,trained_model = '/data/dongchengbo/code/dfdc_1st_Vdcb2.0/weights/retinaface/mobilenet0.25_Final.pth')

    def predict(self,img_raw):
        #输入为cv2 BGR格式
        faces, count, scores_all, ori_coordinates, coordinates, landmarks = self.face_extractor.image_face_extractor(img_raw)
        faces, scores_all, ori_coordinates, coordinates, landmarks = batch_sort(faces, scores_all, ori_coordinates,
                                                                                coordinates, landmarks)
        if len(faces) == 0:
            pdb.set_trace()
            return 0
        inputs = self.transforms(faces,380)
        inputs = inputs.to(self.device)
        with torch.no_grad():
            if self.half:
                output = self.model(inputs.half())
            else:
                output = self.model(inputs)
            output = torch.sigmoid(output).cpu().numpy()
        return np.max(output)

    def initmodel(self):
        print("loading state dict %s"%self.model_path)
        try:
            checkpoint = torch.load(self.model_path, map_location="cpu")
            state_dict = checkpoint.get("state_dict", checkpoint)
            self.model.load_state_dict({re.sub("^module.", "", k): v for k, v in state_dict.items()}, strict=False)
        except:
            import pdb
            pdb.set_trace()
            print("error loading %s"%self.model_path)
        self.model = self.model.to(self.device)
        self.model.eval()
        if self.half:
            self.model = self.model.half()
        del checkpoint


if __name__ == '__main__':
    pred_single = Inference_Single(
        model_path='/data/dongchengbo/VisualSearch/muti_dataset/dfdc_dfv2_ff++_timit_bilibili_celeb5_opensoftware_gan_train/models/dfdc_dfv2_ff++_timit_bilibili_celeb5_opensoftware_gan_val/addmorereal/efficientnet-b7-ns_lr0.005decay0.8_multidata.json/run_0/model_5.pth.tar',
        device=torch.device('cuda'),
        half=True
    )
    pred_single.initmodel()
    # with open('/data/dongchengbo/VisualSearch/GAB_test/xi.txt','r') as f:
    #     imgs = f.readlines()
    # imgs = [each.strip() for each in imgs]
    # labels = [int(each.split()[-1]) for each in imgs]
    # imgs = [each.split()[0] for each in imgs]
    imgs = ['/data/GAB_data/submit_data/images/fake/swap/wangyang_0038_xijinping_0311_1.jpg']
    labels = [0]
    pred = []
    for each in imgs:
        img = cv2.imread(each)
        re = pred_single.predict(img)
        print("%s,fake prob: %2.1f%%"%(each,re*100))
        pred.append(re)
    labels = np.array(labels)
    pred = np.array(pred)
    pred_label = 1.0*(pred > 0.5)
    acc = 1.0*np.sum(pred_label == labels) / labels.shape[0]
    print(acc)
    # fw = open('/data/dongchengbo/VisualSearch/xi.txt', 'w')
    # fw.write(
    #     '\n'.join(['{} {} {}'.format(imgs[i], 1.0 * (pred > 0.5)[i], pred[i]) for i in range(len(imgs))]))
    # fw.close()

