import cv2
import re
from functools import partial
from timm.models.efficientnet import tf_efficientnet_b7_ns
import torch
from torch import nn
from torch.nn.modules.dropout import Dropout
from torch.nn.modules.linear import Linear
from torch.nn.modules.pooling import AdaptiveAvgPool2d
from albumentations import DualTransform, Compose,  PadIfNeeded
from albumentations.pytorch.functional import img_to_tensor
import torchvision.transforms as transforms
from PIL import Image
import pdb
from tqdm import tqdm
from sklearn.metrics import confusion_matrix
import os
import torch.nn.functional as F
import json
"""
模型定义
"""
encoder_params = {
    "tf_efficientnet_b7_ns": {
        "features": 2560,
        "init_op": partial(tf_efficientnet_b7_ns, pretrained=True, drop_path_rate=0.2)
    },
}

class DeepFakeClassifier(nn.Module):
    def __init__(self, encoder, dropout_rate=0.0, num_classes=1) -> None:
        super().__init__()
        self.encoder = encoder_params[encoder]["init_op"]()
        self.avg_pool = AdaptiveAvgPool2d((1, 1))
        self.dropout = Dropout(dropout_rate)
        self.fc = Linear(encoder_params[encoder]["features"], num_classes)

    def forward(self, x):
        x = self.encoder.forward_features(x)
        x = self.avg_pool(x).flatten(1)
        x = self.dropout(x)
        x = self.fc(x)
        return x



INPUT_SIZE = 299
transforms_set = transforms.Compose([
    transforms.Resize((INPUT_SIZE, INPUT_SIZE)),
    transforms.ToTensor(),
])

pretrained_settings = {
    'xception': {
        'imagenet': {
            'url': 'http://data.lip6.fr/cadene/pretrainedmodels/xception-43020ad28.pth',
            'input_space': 'RGB',
            'input_size': [3, 299, 299],
            'input_range': [0, 1],
            'mean': [0.5, 0.5, 0.5],
            'std': [0.5, 0.5, 0.5],
            'num_classes': 1000,
            'scale': 0.8975
            # The resize parameter of the validation transform should be 333, and make sure to center crop at 299x299
        }
    }
}


def conv3x3(in_planes, out_planes, stride=1):
    return nn.Conv2d(in_planes, out_planes, kernel_size=3, stride=stride, padding=1, bias=False)


class SeparableConv2d(nn.Module):
    def __init__(self, in_channels, out_channels, kernel_size=1, stride=1, padding=0, dilation=1, bias=False):
        super(SeparableConv2d, self).__init__()

        self.conv1 = nn.Conv2d(in_channels, in_channels, kernel_size, stride, padding, dilation, groups=in_channels,
                               bias=bias)
        self.pointwise = nn.Conv2d(in_channels, out_channels, 1, 1, 0, 1, 1, bias=bias)

    def forward(self, x):
        x = self.conv1(x)
        x = self.pointwise(x)
        return x


class Block(nn.Module):
    def __init__(self, in_filters, out_filters, reps, strides=1, start_with_relu=True, grow_first=True):
        super(Block, self).__init__()

        if out_filters != in_filters or strides != 1:
            self.skip = nn.Conv2d(in_filters, out_filters, 1, stride=strides, bias=False)
            self.skipbn = nn.BatchNorm2d(out_filters)
        else:
            self.skip = None

        rep = []

        filters = in_filters
        if grow_first:
            rep.append(nn.ReLU(inplace=True))
            rep.append(SeparableConv2d(in_filters, out_filters, 3, stride=1, padding=1, bias=False))
            rep.append(nn.BatchNorm2d(out_filters))
            filters = out_filters

        for i in range(reps - 1):
            rep.append(nn.ReLU(inplace=True))
            rep.append(SeparableConv2d(filters, filters, 3, stride=1, padding=1, bias=False))
            rep.append(nn.BatchNorm2d(filters))

        if not grow_first:
            rep.append(nn.ReLU(inplace=True))
            rep.append(SeparableConv2d(in_filters, out_filters, 3, stride=1, padding=1, bias=False))
            rep.append(nn.BatchNorm2d(out_filters))

        if not start_with_relu:
            rep = rep[1:]
        else:
            rep[0] = nn.ReLU(inplace=False)

        if strides != 1:
            rep.append(nn.MaxPool2d(3, strides, 1))
        self.rep = nn.Sequential(*rep)

    def forward(self, inp):
        x = self.rep(inp)

        if self.skip is not None:
            skip = self.skip(inp)
            skip = self.skipbn(skip)
        else:
            skip = inp

        x += skip
        return x


class Xception(nn.Module):
    """
    Xception optimized for the ImageNet dataset, as specified in
    https://arxiv.org/pdf/1610.02357.pdf
    """

    def __init__(self, num_classes=1000):
        """ Constructor
        Args:
            num_classes: number of classes
        """
        super(Xception, self).__init__()
        self.num_classes = num_classes

        self.conv1 = nn.Conv2d(3, 32, 3, 2, 0, bias=False)
        self.bn1 = nn.BatchNorm2d(32)
        self.relu1 = nn.ReLU(inplace=True)

        self.conv2 = nn.Conv2d(32, 64, 3, bias=False)
        self.bn2 = nn.BatchNorm2d(64)
        self.relu2 = nn.ReLU(inplace=True)
        # do relu here

        self.block1 = Block(64, 128, 2, 2, start_with_relu=False, grow_first=True)
        self.block2 = Block(128, 256, 2, 2, start_with_relu=True, grow_first=True)
        self.block3 = Block(256, 728, 2, 2, start_with_relu=True, grow_first=True)

        self.block4 = Block(728, 728, 3, 1, start_with_relu=True, grow_first=True)
        self.block5 = Block(728, 728, 3, 1, start_with_relu=True, grow_first=True)
        self.block6 = Block(728, 728, 3, 1, start_with_relu=True, grow_first=True)
        self.block7 = Block(728, 728, 3, 1, start_with_relu=True, grow_first=True)

        self.block8 = Block(728, 728, 3, 1, start_with_relu=True, grow_first=True)
        self.block9 = Block(728, 728, 3, 1, start_with_relu=True, grow_first=True)
        self.block10 = Block(728, 728, 3, 1, start_with_relu=True, grow_first=True)
        self.block11 = Block(728, 728, 3, 1, start_with_relu=True, grow_first=True)

        self.block12 = Block(728, 1024, 2, 2, start_with_relu=True, grow_first=False)

        self.conv3 = SeparableConv2d(1024, 1536, 3, 1, 1)
        self.bn3 = nn.BatchNorm2d(1536)
        self.relu3 = nn.ReLU(inplace=True)

        # do relu here
        self.conv4 = SeparableConv2d(1536, 2048, 3, 1, 1)
        self.bn4 = nn.BatchNorm2d(2048)

        self.fc = nn.Linear(2048, num_classes)

    def features(self, input):
        x = self.conv1(input)
        x = self.bn1(x)
        x = self.relu1(x)

        x = self.conv2(x)
        x = self.bn2(x)
        x = self.relu2(x)

        x = self.block1(x)
        x = self.block2(x)
        x = self.block3(x)
        x = self.block4(x)
        x = self.block5(x)
        x = self.block6(x)
        x = self.block7(x)
        x = self.block8(x)
        x = self.block9(x)
        x = self.block10(x)
        x = self.block11(x)
        x = self.block12(x)

        x = self.conv3(x)
        x = self.bn3(x)
        x = self.relu3(x)

        x = self.conv4(x)
        x = self.bn4(x)
        return x

    def logits(self, features):
        x = nn.ReLU(inplace=True)(features)

        x = F.adaptive_avg_pool2d(x, (1, 1))
        x = x.view(x.size(0), -1)
        x = self.fc(x)
        return x

    def forward(self, input):
        x = self.features(input)
        x = self.logits(x)
        return x


def DFDC_infer_face(face, face_model):
    example = cv2.cvtColor(face, cv2.COLOR_BGR2RGB)
    example = Image.fromarray(example)
    example = transforms_set(example)
    classes = face_model(example.unsqueeze(0))
    classes = F.softmax(classes, dim=1)
    score = float(classes[0][1])
    return score


"""
transform
"""
def isotropically_resize_image(img, size, interpolation_down=cv2.INTER_AREA, interpolation_up=cv2.INTER_CUBIC):
    h, w = img.shape[:2]
    if max(w, h) == size:
        return img
    if w > h:
        scale = size / w
        h = h * scale
        w = size
    else:
        scale = size / h
        w = w * scale
        h = size
    interpolation = interpolation_up if scale > 1 else interpolation_down
    resized = cv2.resize(img, (int(w), int(h)), interpolation=interpolation)
    return resized

class IsotropicResize(DualTransform):
    def __init__(self, max_side, interpolation_down=cv2.INTER_AREA, interpolation_up=cv2.INTER_CUBIC,
                 always_apply=False, p=1):
        super(IsotropicResize, self).__init__(always_apply, p)
        self.max_side = max_side
        self.interpolation_down = interpolation_down
        self.interpolation_up = interpolation_up

    def apply(self, img, interpolation_down=cv2.INTER_AREA, interpolation_up=cv2.INTER_CUBIC, **params):
        return isotropically_resize_image(img, size=self.max_side, interpolation_down=interpolation_down,
                                          interpolation_up=interpolation_up)

    def apply_to_mask(self, img, **params):
        return self.apply(img, interpolation_down=cv2.INTER_NEAREST, interpolation_up=cv2.INTER_NEAREST, **params)

    def get_transform_init_args_names(self):
        return ("max_side", "interpolation_down", "interpolation_up")

def create_val_transforms(size=300):
    return Compose([
        IsotropicResize(max_side=size, interpolation_down=cv2.INTER_AREA, interpolation_up=cv2.INTER_CUBIC),
        PadIfNeeded(min_height=size, min_width=size, border_mode=cv2.BORDER_CONSTANT),
    ])

def direct_val(imgs,size):
    #img 输入为RGB顺序
    imgs = [cv2.cvtColor(img, cv2.COLOR_BGR2RGB) for img in imgs]
    transforms = create_val_transforms(size)
    normalize = {"mean": [0.485, 0.456, 0.406],
                 "std": [0.229, 0.224, 0.225]}
    imgs = [img_to_tensor(transforms(image=each)['image'],normalize).unsqueeze(0) for each in imgs]
    imgs = torch.cat(imgs)

    return imgs


"""
inference
"""
class Inference_Single(object):
    def __init__(self,model_path,device,half):
        self.model = DeepFakeClassifier(encoder="tf_efficientnet_b7_ns")
        self.transforms = direct_val
        self.model_path = model_path
        self.device = device
        self.half = half

    def predict(self,img_raw):
        #输入为cv2 BGR格式
        inputs = self.transforms([img_raw],380)
        inputs = inputs.to(self.device)
        with torch.no_grad():
            if self.half:
                output = self.model(inputs.half())
            else:
                output = self.model(inputs)
            output = torch.sigmoid(output).cpu().item()
        return output

    def initmodel(self):
        print("loading state dict %s"%self.model_path)
        try:
            checkpoint = torch.load(self.model_path, map_location="cpu")
            state_dict = checkpoint.get("state_dict", checkpoint)
            self.model.load_state_dict({re.sub("^module.", "", k): v for k, v in state_dict.items()}, strict=False)
        except:
            print("error loading %s"%self.model_path)
        self.model = self.model.to(self.device)
        self.model.eval()
        if self.half:
            self.model = self.model.half()
        del checkpoint


if __name__ == '__main__':
    import numpy as np
    pred_single = Inference_Single(
        model_path= '/data/dongchengbo/VisualSearch/muti_dataset/hunhe_withboss_val/models/boss_val/boss/efficientnet-b7-ns_lr5e-5decay0.8_finetune.json/run_0/model_6.pth.tar',
        #model_path = '/data/dongchengbo/VisualSearch/muti_dataset/dfdc_dfv2_ff++_timit_bilibili_celeb5_opensoftware_gan_train/models/dfdc_dfv2_ff++_timit_bilibili_celeb5_opensoftware_gan_val/addmorereal/efficientnet-b7-ns_lr0.005decay0.8_multidata.json/run_0/model_5.pth.tar',
        device=torch.device('cuda'),
        half=True
    )
    pred_single.initmodel()


    face_detection_model_path_0 = '/data/lvyb/projects/DFDC/our_models/our_model_train/saved_models/GAB_finetune/ckpt_89.pth.tar'

    device = torch.device("cuda:0")
    dfdc_face_model = Xception(num_classes=2).to(device)
    dfdc_face_model = nn.DataParallel(dfdc_face_model)
    dfdc_face_model.load_state_dict(torch.load(face_detection_model_path_0, map_location=device)['state_dict'])
    dfdc_face_model.train(False)



    with open('/data/dongchengbo/VisualSearch/ffbenchmark.txt','r') as f:
        imgs = f.readlines()
    imgs = [each.strip() for each in imgs]
    gts = [int(each.split()[-1]) for each in imgs]
    imgs = [each.split()[0] for each in imgs]
    pred = []
    names = []
    for each in tqdm(imgs):
        img = cv2.imread(each)
        names.append(os.path.split(each)[-1])
        if img is None:
            print(each)
            pred.append(1.0)
            continue

        re1 = pred_single.predict(img)
        re2 = DFDC_infer_face(img,dfdc_face_model)
        re = (re1+re2) /2
        pred.append(re)
    labels = np.array(gts)
    pred = np.array(pred)
    pred_label = 1.0 * (pred > 0.5)
    acc = 1.0 * np.sum(pred_label == labels) / labels.shape[0]
    # print(acc)
    from sklearn.metrics import confusion_matrix

    tn, fp, fn, tp = confusion_matrix(labels, pred_label).reshape(-1)
    sen = float(tp) / (tp + fn + 1e-8)
    spe = float(tn) / (tn + fp + 1e-8)
    print(acc, sen, spe)
        # '''
        # prob = pred_single.predict(img)
        # if prob < 0.6 and prob > 0.2:
        #
        #     #使用opensoft模型粗分类：
        #     prob = gan_opsoft_classfify.predict(img)[0]
        #     pred_label = np.argmax(prob, axis=0)
        #
        #     fake_score = 1 - prob[0]
        #     real_score = prob[0]
        #     #print("opft:\nreal_score:%.3f, fake_score:%.3f"%(real_score,fake_score))
        #
        #
        #     if pred_label == 4:
        #         # import pdb
        #         # pdb.set_trace()
        #         prob = gan_classify.predict(img)[0]
        #         pred_label = np.argmax(prob, axis=0)
        #
        #         if pred_label == 0:
        #             #pdb.set_trace()
        #             real_score = (-0.8*(fake_score + prob[int(pred_label)]) + 1.7) * max(real_score , prob[int(pred_label)])
        #
        #         else:
        #             fake_score = max(fake_score , prob[int(pred_label)])
        #         #print("gan:\nreal_score:%.3f, fake_score:%.3f" % (real_score, fake_score))`
        # else:
        #     real_score = 1-prob
        #     fake_score = prob
        # '''
        # prob = gan_opsoft_classfify.predict(img)[0]
        # pred_label = np.argmax(prob, axis=0)
        #
        # fake_score = 1 - prob[0]
        # real_score = prob[0]
        # # print("opft:\nreal_score:%.3f, fake_score:%.3f"%(real_score,fake_score))
        #
        # if pred_label == 4:
        #     # import pdb
        #     # pdb.set_trace()
        #     prob = gan_classify.predict(img)[0]
        #     pred_label = np.argmax(prob, axis=0)
        #
        #     if pred_label == 0:
        #         # pdb.set_trace()
        #         real_score = (-0.8 * (fake_score + prob[int(pred_label)]) + 1.7) * max(real_score,
        #                                                                                prob[int(pred_label)])
        #
        #     else:
        #         fake_score = max(fake_score, prob[int(pred_label)])
        # if max(real_score,fake_score) > 0.2 and max(real_score,fake_score) < 0.7:
        #     prob = pred_single.predict(img)
        #     real_score2 = np.mean([real_score, 1 - prob])
        #     fake_score2 = np.mean([fake_score, prob])
        #     # print("gan:\nreal_score:%.3f, fake_score:%.3f" % (real_score, fake_score))`
        # # dcb model
        # #prob = pred_single.predict(img)
        # # labels = ['real','fake']
        # # result = labels[1*(prob >= 0.5)]
        # #print("dcb:\nreal_score:%.3f, fake_score:%.3f" % (1-prob, prob))
        # #real_score2 = np.mean([((real_score - 1+prob) + 0.8) * real_score, 1-prob])
        # #real_score2 = np.var([real_score,1-prob]) * np.mean([real_score,1-prob])
        # #fake_score2 = np.var([fake_score,prob]) * np.mean([fake_score,prob])
        # #fake_score2 = np.mean([((fake_score - prob) + 0.8) * fake_score,prob])
        # else:
        #     fake_score2 =fake_score
        #     real_score2 = real_score
        # # if real_score2== 0 or fake_score2 == 0:
        # #     pdb.set_trace()
        # #print("final:\nreal_score:%.3f, fake_score:%.3f" % (real_score2, fake_score2))
        #
        # if real_score2 > fake_score2:
        #     re = 1-real_score2
        # else:
        #     re = fake_score2
        # #temp = torch.softmax(torch.tensor([real_score2,fake_score2]),dim=0)
        # pred.append(re)
        # #print("%s,fake prob: %2.1f%%\n"%(each,re*100))

    gts = np.array(gts)
    pred = np.array(pred)
    pred_label = 1.0*(pred > 0.5)
    acc = 1.0*np.sum(pred_label == gts) / gts.shape[0]
    tn, fp, fn, tp = confusion_matrix(gts, pred_label).reshape(-1)
    sen = float(tp) / (tp + fn + 1e-8)
    spe = float(tn) / (tn + fp + 1e-8)
    print(acc,sen,spe)
    fw = open('/data/dongchengbo/VisualSearch/xi.txt', 'w')
    fw.write(
        '\n'.join(['{} {} {}'.format(imgs[i], 1.0 * (pred > 0.5)[i], pred[i]) for i in range(len(imgs))]))
    fw.close()
    probs = pred.reshape(-1)
    results = {}
    label = ['real', 'fake']
    for i in range(len(names)):
        results[os.path.split(names[i])[-1]] = label[probs[i] > 0.5]
    with open('/data/dongchengbo/VisualSearch/xi_bench.json', "w", encoding='utf8') as f:
        json.dump(results, f, ensure_ascii=False, indent=4)
