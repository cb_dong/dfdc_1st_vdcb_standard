from torch.autograd import Variable
from tools.utils import AverageMeter, Progbar
from tools.eval import predict_set, evaluate
import torch
import json
import pdb
import time
from apex import amp

def validate(args, data_val, bce_best, model, model_dir, current_epoch, summary_writer,conf):
    print("Test phase")
    if args.debug:
        pdb.set_trace()
    model = model.eval()
    probs, gt_labels, names = predict_set([model],data_val,{'run_type':'val','debug':args.debug})
    matrix = evaluate(gt_labels, probs > conf['pos_th'], probs)
    bce = matrix['bce']

    #bce, probs, targets = validate(model, data_loader=data_val)
    if args.local_rank == 0:
        summary_writer.add_scalar('val/bce', float(bce), global_step=current_epoch)
        if bce < bce_best:
            print("Epoch {} improved from {} to {}".format(current_epoch, bce_best, bce))
            torch.save({
                'epoch': current_epoch + 1,
                'state_dict': model.state_dict(),
                'bce_best': bce,
            }, model_dir + "/model_best_dice.pth.tar")
            bce_best = bce

        torch.save({
            'epoch': current_epoch + 1,
            'state_dict': model.state_dict(),
            'bce_best': bce_best,
        }, model_dir + '/model_last.pth.tar')

        print("Epoch: {} bce: {}, bce_best: {}".format(current_epoch, bce, bce_best))
        print(matrix)
    return bce_best


def train_epoch(current_epoch, loss_function, model, optimizer, scheduler, train_data_loader, summary_writer, conf,
                local_rank, debug):
    #存储平均值
    progbar = Progbar(len(train_data_loader.dataset), stateful_metrics=['epoch', 'config','lr'])
    batch_time = AverageMeter()
    end = time.time()
    losses = AverageMeter()
    fake_losses = AverageMeter()
    real_losses = AverageMeter()
    max_iters = conf['optimizer']['schedule']['params']['max_iter']
    print("training epoch {}".format(current_epoch))
    model.train()

    for i, (labels, imgs, img_path) in enumerate(train_data_loader):
        numm = imgs.shape[0]
        optimizer.zero_grad()
        imgs = imgs.reshape((-1,imgs.size(-3),imgs.size(-2), imgs.size(-1)))
        imgs = Variable(imgs, requires_grad=True).cuda()

        labels = labels.reshape(-1)
        labels = labels.cuda().float()
        out_labels = model(imgs)

        fake_loss = 0
        real_loss = 0
        fake_idx = labels > 0.5
        real_idx = labels <= 0.5
        if torch.sum(fake_idx * 1) > 0:
            fake_loss = loss_function(out_labels[fake_idx].reshape(-1), labels[fake_idx])
        if torch.sum(real_idx * 1) > 0:
            real_loss = loss_function(out_labels[real_idx].reshape(-1), labels[real_idx])
        #loss = (1.2 * fake_loss * sum(fake_idx) + real_loss * sum(real_idx)) / len(labels)

        if fake_loss > real_loss:
            loss = (1.2 * fake_loss * sum(fake_idx) + real_loss * sum(real_idx)) / len(labels)
        elif fake_loss < real_loss:
            loss = (fake_loss * sum(fake_idx) + 1.2 * real_loss * sum(real_idx)) / len(labels)
        else:
            loss = (fake_loss * sum(fake_idx) + real_loss * sum(real_idx)) / len(labels)

        losses.update(loss.item(), imgs.size(0))
        fake_losses.update(0 if fake_loss == 0 else fake_loss.item(), imgs.size(0))
        real_losses.update(0 if real_loss == 0 else real_loss.item(), imgs.size(0))
        summary_writer.add_scalar('train/loss', loss.item(), global_step=i + current_epoch * max_iters)
        summary_writer.add_scalar('train/lr',float(scheduler.get_lr()[-1]), global_step=i + current_epoch * max_iters)

        if conf['fp16']:
            with amp.scale_loss(loss, optimizer) as scaled_loss:
                scaled_loss.backward()
        else:
            loss.backward()
        torch.nn.utils.clip_grad_norm_(amp.master_params(optimizer), 1)
        optimizer.step()
        torch.cuda.synchronize()

        batch_time.update(time.time() - end)
        end = time.time()

        if conf["optimizer"]["schedule"]["mode"] in ("step", "poly"):
            scheduler.step(i + current_epoch * max_iters)
            if (i == max_iters - 1) or debug:
                break
        progbar.add(numm, values=[('epoch', current_epoch),
                                          ('loss', losses.avg),
                                          ("lr",float(scheduler.get_lr()[-1])),
                                          ("f",fake_losses.avg),
                                          ("r",real_losses.avg)])


    if conf["optimizer"]["schedule"]["mode"] == "epoch":
        scheduler.step(current_epoch)
    if local_rank == 0:
        for idx, param_group in enumerate(optimizer.param_groups):
            lr = param_group['lr']
            summary_writer.add_scalar('group{}/lr'.format(idx), float(lr), global_step=current_epoch)
        #summary_writer.add_scalar('train/loss', float(losses.avg), global_step=current_epoch)

if __name__ == '__main__':
    pass