import pdb
import torch
import numpy as np
import cv2
import time
import skimage
from sklearn.metrics import roc_auc_score, average_precision_score, confusion_matrix, log_loss
from tools.utils import Progbar,AverageMeter
from tools.transforms import direct_val

kernel = np.ones((380, 380)).astype(np.float32)
for i in range(kernel.shape[0]):
    for j in range(kernel.shape[1]):
        kernel[i, j] = np.exp(-((i - (kernel.shape[0] - 1) / 2) ** 2 + (j - (kernel.shape[1] - 1) / 2) ** 2) / 2 / 20 ** 2)

def predict_imgs(imgs,nets,half):
    temp = []
    with torch.no_grad():
        for net in nets:
            if half:
                output = net(imgs.half())
            else:
                output = net(imgs)
            output = torch.sigmoid(output).cpu().numpy()
            temp.append(output)
        output = np.mean(temp, axis=0)  # .reshape(-1)
    return output


def confident_strategy(pred, t=0.8,var=True,high=True):
    pred = np.array(pred)
    if not high:
        if (var and np.var(pred) > 0.15):
            return np.mean(pred[pred > t])
        return np.mean(pred)
    sz = len(pred)
    fakes = np.count_nonzero(pred > t)
    # 11 frames are detected as fakes with high probability
    if (fakes > sz // 2.5 and fakes > 11) or (var and np.var(pred) > 0.15):
        return np.mean(pred[pred > t])
    elif np.count_nonzero(pred < 0.2) > 0.9 * sz:
        return np.mean(pred[pred < 0.2])
    else:
        return np.mean(pred)


def predict_set(nets, dataloader, runtime_params):
    run_type = runtime_params['run_type']
    #net = net.eval()
    progbar = Progbar(len(dataloader.dataset), stateful_metrics=['run-type'])
    batch_time = AverageMeter()
    names = []
    probs = np.array([])
    gt_labels = np.array([])
    with torch.no_grad():
        for i, (labels, imgs, img_paths) in enumerate(dataloader):
            s_time = time.time()
            imgs = imgs.cuda()

            names.extend(img_paths)
            temp = []

            for net in nets:
                if 'half' in runtime_params.values():
                    output = net(imgs.half())
                else:
                    output = net(imgs)
                output = torch.sigmoid(output).cpu().numpy().reshape(-1)
                temp.append(output)
            output = np.mean(temp, axis=0).reshape(-1)
            probs = np.concatenate((probs,output),axis=0)
            gt_labels = np.concatenate((gt_labels,labels.data.numpy().reshape(-1)),axis=0)
            assert gt_labels.shape == probs.shape

            progbar.add(imgs.size(0), values=[('run-type', run_type)])  # ,('batch_time', batch_time.val)])
            batch_time.update(time.time() - s_time)
            if runtime_params['debug'] and i:
                break
    return probs, gt_labels, names

def check_blur(img,th=0.5):
    img = cv2.resize(img, (380, 380))
    img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    imarr = np.array(img)

    fft = np.fft.fft2(imarr)
    fft = np.fft.fftshift(fft)
    fft *= kernel
    fft = np.fft.ifftshift(fft)
    ifft = np.fft.ifft2(fft)

    ifft = np.real(ifft)
    max = np.max(ifft)
    min = np.min(ifft)

    res = 255 * (ifft - min) / (max - min)
    ssim = skimage.measure.compare_ssim(img, res)
    return ssim < th

def evaluate(gt_labels, pred_labels, scores):

    n = gt_labels.shape[0]
    fake_idx = gt_labels > 0.5
    real_idx = gt_labels < 0.5
    real_loss = 0
    fake_loss = 0
    if np.sum(real_idx * 1) > 0:
        real_loss = log_loss(fake_idx[real_idx], scores[real_idx], labels=[0, 1])
    if np.sum(fake_idx * 1) > 0:
        fake_loss = log_loss(fake_idx[fake_idx], scores[fake_idx], labels=[0, 1])

    print("{}fake_loss".format(""), fake_loss)
    print("{}real_loss".format(""), real_loss)

    bce = (fake_loss + real_loss) / 2
    if fake_loss * real_loss == 0:
        n += 1
        temp = [gt_labels,pred_labels,scores]

        for i in range(3):
            temp[i] = temp[i].tolist()
            temp[i].append((fake_loss==0)*1)
            temp[i] = np.array(temp[i])
        gt_labels, pred_labels, scores = temp

    tn, fp, fn, tp = confusion_matrix(gt_labels, pred_labels).reshape(-1)
    assert ((tn + fp + fn + tp) == n)
    sen = float(tp) / (tp + fn + 1e-8)
    spe = float(tn) / (tn + fp + 1e-8)
    f1 = 2.0 * sen * spe / (sen + spe)
    acc = float(tn + tp) / n

    auc = roc_auc_score(gt_labels, scores)
    ap = average_precision_score(gt_labels, scores)

    return {'bce':bce,'auc': auc, 'ap': ap, 'sen': sen, 'spe': spe, 'f1': f1, 'acc': acc}


def batch_sort(faces, scores_all, ori_coordinates, coordinates, landmarks, blur=True):
    #按照面积划分
    sort_base = [(face_coord[3] - face_coord[1]) * (face_coord[2] - face_coord[0]) for face_coord in ori_coordinates]
    sort_index = np.argsort(sort_base)
    sort_index = sort_index[::-1]

    faces_new = []
    scores_all_new = []
    ori_coordinates_new = []
    coordinates_new = []
    landmarks_new = []
    for index in sort_index:
        # if blur and check_blur(faces[index],th=0.5):
        #     continue
        faces_new.append(faces[index])
        scores_all_new.append(scores_all[index])
        ori_coordinates_new.append(ori_coordinates[index])
        coordinates_new.append(coordinates[index])
        landmarks_new.append(landmarks[index])
    return faces_new, scores_all_new, ori_coordinates_new, coordinates_new, landmarks_new


def pred_video(modelss,pro_result,frame_chosen,face_extractor,capture,blur):
    for index in range(len(frame_chosen)):
        capture.set(cv2.CAP_PROP_POS_FRAMES, frame_chosen[index])
        ret, frame = capture.read()
        if ret:
            faces, count, scores_all, ori_coordinates, coordinates, landmarks = face_extractor.image_face_extractor(frame)
            faces, scores_all, ori_coordinates, coordinates, landmarks = batch_sort(faces, scores_all, ori_coordinates,
                                                                                    coordinates, landmarks, blur=blur)
            if len(faces) > 0:
                with torch.no_grad():
                    inputs = direct_val(faces, 380)
                    inputs = inputs.cuda()
                    outputs = predict_imgs(nets=modelss, imgs=inputs, half=True)
                    pred = np.max(outputs)
                    pro_result.append(pred)
            else:
                pass
                # print("%s-%d found no face"%(video_path_name, frame_chosen[index]))
        else:
            pass
            # print("%s-%d can not be loaded" % (video_path_name, frame_chosen[index]))
    return pro_result

