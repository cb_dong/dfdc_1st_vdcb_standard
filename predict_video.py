import argparse
import os
import re
import torch
import cv2
import pdb
import random
from models.classifier import DeepFakeClassifier
from torch.backends import cudnn
from tools.utils import read_annotations, Progbar
from tools.eval import evaluate, confident_strategy, pred_video

from torch.backends import cudnn
import numpy as np
import warnings
from models.retina_face_extractor import RetinaFaceExtractorVideo
warnings.filterwarnings("ignore")

os.environ["MKL_NUM_THREADS"] = "1"
os.environ["NUMEXPR_NUM_THREADS"] = "1"
os.environ["OMP_NUM_THREADS"] = "1"

torch.backends.cudnn.benchmark = True
cv2.ocl.setUseOpenCL(False)
cv2.setNumThreads(0)

def parse_args():
    parser = argparse.ArgumentParser(description='predict_img')
    parser.add_argument('--models', nargs='+', required=True, help="checkpoint files")
    parser.add_argument('--test_dir', nargs='+', required=True, help="path to directory with videos")
    parser.add_argument('--gpu', default='0', type=str)
    args = parser.parse_args()
    return args


if __name__ == '__main__':
    args = parse_args()
    os.environ['CUDA_DEVICE_ORDER'] = 'PCI_BUS_ID'
    os.environ["CUDA_VISIBLE_DEVICES"] = args.gpu
    cudnn.benchmark = True

    face_extractor = RetinaFaceExtractorVideo(torch.device('cuda:%s'%0),trained_model='/data/dongchengbo/code/dfdc_1st_Vdcb2.0/weights/retinaface/mobilenet0.25_Final.pth')
    modelss = []
    model_paths = [model for model in args.models]
    for path in model_paths:
        model = DeepFakeClassifier(encoder="tf_efficientnet_b7_ns")
        print("loading state dict {}".format(path))
        checkpoint = torch.load(path, map_location="cpu")
        state_dict = checkpoint.get("state_dict", checkpoint)
        model.load_state_dict({re.sub("^module.", "", k): v for k, v in state_dict.items()}, strict=False)
        model.eval()
        model.cuda()
        del checkpoint
        modelss.append([model.half()])
    print("load models finish")

    test_dirs = [each for each in args.test_dir]
    data_name = [os.path.basename(each).split('.')[0] for each in test_dirs]
    #[var,blur,confidence]
    type = '111'
    for each in list(zip(test_dirs, data_name)):
        annotations = read_annotations(each[0])
        save_dir = os.path.split(each[0])[0]
        print("begin to pred %s"%each[1])
        # test_samples = [x.strip() for x in open(args.test_dir).readlines() if x.strip()]
        # annotations = [(x,0) for x in test_samples]
        videos = [each[0] for each in annotations]
        labels = [each[1] for each in annotations]
        scores = []
        gt_labels = []
        names = []
        progbar = Progbar(len(videos), stateful_metrics=['video','score','label'])

        for (ix_video_,video_path_name) in enumerate(videos):
            capture = cv2.VideoCapture(video_path_name)
            frame_count = int(capture.get(cv2.CAP_PROP_FRAME_COUNT))
            if frame_count <= 0:
                print("%s is not available"%(video_path_name))
                progbar.add(1, values=[('video', os.path.basename(video_path_name)), ('score', 0),
                                       ('label', labels[ix_video_])])
                continue
            pro_results = [[] for i in range(len(modelss))]
            count = 0
            while len(pro_results[0]) < 16:
                if count >5:
                    break
                frame_chosen = np.linspace(frame_count // 10 + np.random.randint(-20,20),
                                           9 * (frame_count - 1) // 10 + np.random.randint(-20,20),
                                           32, endpoint=True, dtype=np.int)
                pro_results = [pred_video(modelss[sub_ix],
                                          pro_results[sub_ix],
                                          frame_chosen,
                                          face_extractor,
                                          capture,
                                          int(type[1])) for sub_ix in range(len(modelss))]
                count += 1
            if len(pro_results[0]) == 0:
                print("%s found no frames"%(video_path_name))
                progbar.add(1, values=[('video', os.path.basename(video_path_name)), ('score', 0),
                                       ('label', labels[ix_video_])])
                continue
            merge_result = np.mean(pro_results,axis=0).tolist()
            pro_results.append(merge_result)
            score = [confident_strategy(pro_result,var=int(type[0]),high=int(type[2])) for pro_result in pro_results]
            names.append(video_path_name)
            scores.append(score)
            gt_labels.append(labels[ix_video_])
            progbar.add(1, values=[('video',os.path.basename(video_path_name)),('score', score),('label',labels[ix_video_])])
        #pdb.set_trace()
        scores = np.array(scores).transpose(1, 0)
        for model_ix in range(len(modelss)):
            score_each = np.array(scores[model_ix]).reshape(-1)
            gt_labels = np.array(gt_labels).reshape(-1)
            metrix = evaluate(gt_labels, 1.0*(score_each > 0.5), score_each)
            print("model: %s\ndata: %s" % (os.path.basename(model_paths[model_ix]), each[1]))
            print(metrix)
            print('\n')
            fw = open("/data/dongchengbo/VisualSearch/all_results/video/%s.txt" % each[1], 'w')
            #fw = open('%s/%s_%s.txt' % (save_dir, os.path.basename(model_paths[model_ix]), os.path.basename(each[1])), 'w')
            fw.write(
                '\n'.join(
                    ["%s %s %s"%(names[i], gt_labels[i], score_each[i]) for i in range(len(names))]
            ))
            fw.close()
            print("result save at %s" % ("/data/dongchengbo/VisualSearch/all_results/video/%s.txt" % each[1]))
            #print("result save at %s/%s_%s.txt"%(save_dir, os.path.basename(model_paths[model_ix]), os.path.basename(each[1])))

        # score_merge = np.array(scores[-1]).reshape(-1)
        # gt_labels = np.array(gt_labels).reshape(-1)
        # metrix = evaluate(gt_labels, 1.0 * (score_merge > 0.5), score_merge)
        # print("merge\ndata: %s" % (each[1]))
        # print(metrix)
        # fw = open('%s/merge_%s.txt' % (save_dir, os.path.basename(each[1])), 'w')
        # fw.write(
        #     '\n'.join(
        #         ['{} {} {}'.format(names[i], 1.0 * (score_merge > 0.5)[i], score_merge[i]) for i in range(len(names))]))
        # fw.close()
        # print("merge result save at: %s" % ('%s/merge_%s.txt' % (save_dir, os.path.basename(each[1]))))
        # merge_score = np.array(scores)[-1].reshape(-1)
        # merge_score = np.array(scores)[-1].reshape(-1)
        # metrix = evaluate(gt_labels, 1.0 * (merge_score > 0.5), merge_score)
        # print("model: merge\ndata: %s" %each[1])
        # print(metrix)
        # np.save("/data/dongchengbo/VisualSearch/GAB_test/merge_%s"%(each[1]), merge_score)
        del scores,gt_labels,pro_results
