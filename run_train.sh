#!/usr/bin/env bash

data_path=$HOME/VisualSearch/open_software
run_id=0
config_name=efficientnet-b7-ns_lr0.01decay0.8_.json
train_collection=open_software_byactor_train
val_collection=open_software_byactor_val
test_collection=open_software_byactor_test

GPU=$1
prefix=$2
python3 train_classifier.py \
--data_path $data_path \
--train_collection $train_collection \
--val_collection $val_collection \
--run_id $run_id \
--config_name $config_name \
--gpu $GPU \
--resume test \
--seed 777 \
--prefix $prefix
