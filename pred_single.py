import cv2
from tools.transforms import direct_val
import torch
from models.classifier import DeepFakeClassifier
import re


class Pred_Single(object):
    def __init__(self,model_path,device,half):
        self.model = DeepFakeClassifier(encoder="tf_efficientnet_b7_ns")
        self.transforms = direct_val
        self.model_path = model_path
        self.device = device
        self.half = half

    def predict(self,img_raw):
        #输入为cv2 BGR格式
        inputs = self.transforms([img_raw],380)
        inputs = inputs.to(self.device)
        with torch.no_grad():
            if self.half:
                output = self.model(inputs.half())
            else:
                output = self.model(inputs)
            output = torch.sigmoid(output).cpu().item()
        return output

    def initmodel(self):
        print("loading state dict %s"%self.model_path)
        try:
            checkpoint = torch.load(self.model_path, map_location="cpu")
            state_dict = checkpoint.get("state_dict", checkpoint)
            self.model.load_state_dict({re.sub("^module.", "", k): v for k, v in state_dict.items()}, strict=False)
        except:
            print("error loading %s"%self.model_path)
        self.model = self.model.to(self.device)
        self.model.eval()
        if self.half:
            self.model = self.model.half()
        del checkpoint

if __name__ == '__main__':
    pred_single = Pred_Single(
        model_path='/data/dongchengbo/code/dfdc_1st_Vdcb/weights/best/fix_lr0.01_decay0.8_resume2DeepFakeClassifier_tf_efficientnet_b7_ns_0_last',
        device=torch.device('cuda'),
        half=True
    )
    pred_single.initmodel()

    img = cv2.imread("/data/dongchengbo/code/Sanders_0300.png")
    re = pred_single.predict(img)
    print("fake prob: %2.1f%%"%(re*100))